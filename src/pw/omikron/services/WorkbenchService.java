package pw.omikron.services;

import pw.omikron.backend.Workbench;

public abstract class WorkbenchService {
	
	private static volatile Workbench instance;
	
	public static Workbench getInstance() {
		if(instance == null) {
			instance = FileService.loadWorkbench();
			if(instance == null) {
				instance = new Workbench();
				FileService.saveWorkbench(instance);
			}
		}
		return instance;
	}

}
