package pw.omikron.services;

import java.util.ArrayList;

import pw.omikron.backend.Category;
import pw.omikron.backend.DataType;
import pw.omikron.backend.Part;

public abstract class SearchService {
	
	public static ArrayList<Part> getPartsFromCategory(Category cat) {
		ArrayList<Part> partList = new ArrayList<Part>();
		
		for(Part p : WorkbenchService.getInstance().partList) {
			if(p.cat == cat)
				partList.add(p);
		}
		
		return partList;
	}
	
	public static ArrayList<Part> searchForProperty(String str) {
		ArrayList<String[]> commandList = processString(str);
		return findParts(commandList);
	}
	
	private static ArrayList<Part> findParts(ArrayList<String[]> commandList) {
		ArrayList<Part> partList = new ArrayList<Part>();
		
		for(Part p : WorkbenchService.getInstance().partList) {
			boolean partMatches = true;
			for(String[] cmd : commandList) {
				if(p.isEntryInProperties(cmd[0])) {
					switch((DataType)p.getEntry(cmd[0]).obj2) {
					case BOOLEAN:
						if(Boolean.parseBoolean(cmd[1]) != (Boolean) p.getEntry(cmd[0]).obj1)
							partMatches = false;
						break;
					case DOUBLE:
						if(Double.parseDouble(cmd[1]) != (Double) p.getEntry(cmd[0]).obj1)
							partMatches = false;
						break;
					case INTEGER:
						if(Integer.parseInt(cmd[1]) != (Integer) p.getEntry(cmd[0]).obj1)
							partMatches = false;
						break;
					case STRING:
						if(cmd[1] != (String) p.getEntry(cmd[0]).obj1)
							partMatches = false;
						break;
					default:
						break;
					
					}
				}
			}
			
			if(partMatches) {
				partList.add(p);
			}
		}
		
		return partList;
	}
	 
	private static ArrayList<String[]> processString(String str) {
		ArrayList<String[]> commandList = new ArrayList<String[]>();
		str = str.toLowerCase();
		str = str.replaceAll("\\s", "");
		String[] commands = str.split(";");
		
		for(String s : commands) {
			commandList.add(s.split("="));
		}
		
		boolean valid = true;
		for(String[] s : commandList) {
			if(s.length != 2)
				valid = false;
		}
		
		if(valid)
			return commandList;
		
		ErrorService.throwError("Invalid Query!");
		return null;
	}

}
