package pw.omikron.services;

import pw.omikron.frontend.ErrorFrame;

public abstract class ErrorService {
	
	public static void throwError(String errorMSG) {
		new ErrorFrame(errorMSG);
	}

}
