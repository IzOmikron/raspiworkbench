package pw.omikron.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import pw.omikron.backend.Workbench;

public abstract class FileService {
	
	public static final String workbenchPath = "workbench";
	public static final String dirPath = System.getProperty("user.home")+"/.raspiworkbench/";
	
	public static synchronized Workbench loadWorkbench() {
		checkForInit();
		
		if(!new File(dirPath+workbenchPath).exists())
			return null;
		
		try {
			ObjectInputStream os = new ObjectInputStream(new FileInputStream(new File(dirPath+workbenchPath)));
			
			Object o = os.readObject();
			
			os.close();
			return (Workbench)o;
		} catch(IOException | ClassNotFoundException e) {
			ErrorService.throwError("Error in loading Workbench File!");
		}
		
		return null;
	}
	
	public static synchronized void saveWorkbench(Workbench wb) {
		checkForInit();
		
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(new File(dirPath+workbenchPath)));
			
			os.writeObject(wb);
			
			os.close();
		} catch(IOException e) {
			ErrorService.throwError("Error in saving Workbench File!");
		}
	}
	
	public static synchronized  void checkForInit() {
		try {
			File f = new File(dirPath);
			if(!f.exists() || !f.isDirectory()) {
				f.mkdirs();
			}
		} catch(Exception e) {
			ErrorService.throwError("Error in FileService. Cannot Access Files");
		}
	}
}
