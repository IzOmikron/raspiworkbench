package pw.omikron.services;

import pw.omikron.frontend.MainFrame;
import pw.omikron.music.MusicService;

public abstract class InitService {

	public static void main(String[] args) {
		Thread musicThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				new MusicService();
			}
		});
		musicThread.start();
		
		Thread workbenchThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				//new MainFrame();
			}
		});
		workbenchThread.start();
	}
}
