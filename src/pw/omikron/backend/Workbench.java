package pw.omikron.backend;

import java.io.Serializable;
import java.util.ArrayList;

public class Workbench implements Serializable {
	
	private static final long serialVersionUID = -1825810595776913229L;
	
	public ArrayList<Part> partList;
	public ArrayList<Category> categoryList;
	
	public Workbench() {
		partList = new ArrayList<Part>();
		categoryList = new ArrayList<Category>();
	}
}
