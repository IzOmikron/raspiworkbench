package pw.omikron.backend;

public enum DataType {

	INTEGER,
	DOUBLE,
	STRING,
	BOOLEAN
}
