package pw.omikron.backend;

import java.util.HashMap;

import pw.omikron.services.ErrorService;

public class Part {

	private volatile HashMap<String, Tuple> propertyMap;
	public int count;
	public Category cat;
	
	public void addEntry(String entryName, Object value, DataType dt) {
		if(!propertyMap.containsKey(entryName))
			propertyMap.put(entryName, new Tuple(value, dt));
		else
			ErrorService.throwError("Entry is already in Properties of Part!");
	}
	
	public void updateEntry(String entryName, Object value, DataType usedDataType) {
		if(propertyMap.containsKey(entryName))
			propertyMap.replace(entryName, new Tuple(value, usedDataType));
		else
			ErrorService.throwError("Entry is not in Properties of Part!");
	}
	
	public Tuple getEntry(String entryName) {
		if(propertyMap.containsKey(entryName))
			return propertyMap.get(entryName);
		else
			ErrorService.throwError("Entry is not in Properties of Part!");
		return null;
	}
	
	public String[] getEntryList() {
		return (String[]) propertyMap.keySet().toArray();
	}
	
	public void removeEntry(String entryName) {
		if(propertyMap.containsKey(entryName))
			propertyMap.remove(entryName);
		else
			ErrorService.throwError("Entry is not in Properties of Part!");
	}
	
	public boolean isEntryInProperties(String entryName) {
		return propertyMap.containsKey(entryName);
	}
}