package pw.omikron.frontend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants; 

public class ErrorFrame {
	
	private JFrame frame = new JFrame();
	private JPanel contentPane = new JPanel(null);
	private Font f = new Font("Lato#14", Font.PLAIN, 14);
	
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	public ErrorFrame(String errorMSG) {
		frame.pack();
		frame.setIconImage(new ImageIcon("img/ic.png").getImage());
		frame.setTitle("Raspi Workbench - Info");
		frame.setSize(400, 250);
		frame.setResizable(false);
		frame.setContentPane(contentPane);
		frame.setLocation(screenSize.width/2-200, screenSize.height/2-125);
		
		init(errorMSG);
		
		frame.setVisible(true);
	}
	
	private void init(String errorMSG) {
		JLabel lblErrorIMG = new JLabel(new ImageIcon("img/error.png"));
		lblErrorIMG.setBounds(20, 20, 100, 100);
		contentPane.add(lblErrorIMG);
		
		JLabel lblErrorMSG = new JLabel("<html>" + errorMSG + "</html>");
		lblErrorMSG.setBounds(140, 20, 200, 100);
		lblErrorMSG.setFont(f);
		lblErrorMSG.setForeground(Color.DARK_GRAY);
		contentPane.add(lblErrorMSG);
		
		JLabel lblOK = new JLabel("OK");
		lblOK.setBounds(320, 180, 60, 30);
		lblOK.setBackground(Color.BLACK);
		lblOK.setOpaque(true);
		lblOK.setHorizontalAlignment(SwingConstants.CENTER);
		lblOK.setForeground(Color.WHITE);
		lblOK.setFont(f.deriveFont(Font.BOLD));
		lblOK.addMouseListener(new MouseListener() {
			@Override public void mouseClicked(MouseEvent arg0) { }
			@Override public void mousePressed(MouseEvent arg0) { }
			@Override
			public void mouseReleased(MouseEvent arg0) {
				frame.dispose();
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				lblOK.setBackground(Color.BLACK);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblOK.setBackground(Color.DARK_GRAY);
			}
			
		});
		contentPane.add(lblOK);
	}

}
