package pw.omikron.frontend;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import pw.omikron.services.ErrorService;
import pw.omikron.services.SearchService;

public class MainFrame {
	
	private JFrame frame = new JFrame();
	private JPanel contentPane = new JPanel(null);
	private JPanel categoryPane = new JPanel(null);
	private JPanel partPane = new JPanel(null);
	private JPanel addCategoryPane = new JPanel(null);
	private JPanel addPartPane = new JPanel(null);
	private JScrollPane lowerCategoryPane = new JScrollPane();
	private JScrollPane lowerPartPane = new JScrollPane();
	
	private JTextField tfSearch;
	
	
	private ImageIcon addIcon = new ImageIcon("img/add.png");
	
	private Font f = new Font("Lato#14", Font.PLAIN, 14);
	
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	public MainFrame() {
		frame.pack();
		frame.setSize(screenSize);
		frame.setResizable(false);
		frame.setTitle("Raspi Workbench");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(contentPane);
		frame.setIconImage(new ImageIcon("img/ic.png").getImage());
		
		init();
	
		frame.setVisible(true);
	}
	
	private void init() {
		
		//------------------------------------
		//			CategoryPane
		//------------------------------------
		JLabel lblSearch = new JLabel("Search:");
		lblSearch.setBounds(20, 20, 80, 20);
		lblSearch.setFont(f.deriveFont(16f).deriveFont(Font.BOLD));
		lblSearch.setForeground(Color.WHITE);
		categoryPane.add(lblSearch);
		
		tfSearch = new JTextField();
		tfSearch.setBounds(20, 50, screenSize.width/6-50, 30);
		tfSearch.setBackground(new Color(70, 70, 70));
		tfSearch.setForeground(Color.WHITE);
		tfSearch.setFont(f.deriveFont(Font.BOLD));
		tfSearch.setBorder(null);
		tfSearch.addKeyListener(new KeyListener() {
			@Override public void keyTyped(KeyEvent arg0) { }
			@Override public void keyPressed(KeyEvent arg0) { }
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					if(tfSearch.getText().contains(";") && tfSearch.getText().contains("=")) {
						SearchService.searchForProperty(tfSearch.getText());
						//TODO Show searchQueryInMainFrame
						tfSearch.setText("");
					} else {
						ErrorService.throwError("Check Search Query.");
					}
				}
			}
		});
		categoryPane.add(tfSearch);
		
		JLabel lblSearchSeperatator = new JLabel();
		lblSearchSeperatator.setBounds(0, 100, screenSize.width/6, 1);
		lblSearchSeperatator.setBackground(Color.BLACK);
		lblSearchSeperatator.setOpaque(true);
		categoryPane.add(lblSearchSeperatator);
		
		JLabel lblAddCategoryImage = new JLabel(new ImageIcon("img/add.png"));
		lblAddCategoryImage.setBounds(0, 0, screenSize.height/15, screenSize.height/15);
		lblAddCategoryImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddCategoryImage.setVerticalAlignment(SwingConstants.CENTER);
		addCategoryPane.add(lblAddCategoryImage);
		
		JLabel lblAddCategoryText = new JLabel("Add Category");
		lblAddCategoryText.setFont(f.deriveFont(Font.BOLD).deriveFont(16f));
		lblAddCategoryText.setForeground(Color.WHITE);
		lblAddCategoryText.setBounds(screenSize.height/15, 0, screenSize.width/6-screenSize.height/15, screenSize.height/15);
		addCategoryPane.add(lblAddCategoryText);
		
		JLabel lblAddCategorySeperator = new JLabel();
		lblAddCategorySeperator.setOpaque(true);
		lblAddCategorySeperator.setBackground(Color.BLACK);
		lblAddCategorySeperator.setBounds(0, 101+screenSize.height/15, screenSize.width/6, 1);
		categoryPane.add(lblAddCategorySeperator);
		
		addCategoryPane.setBounds(0, 101, screenSize.width/6, screenSize.height/15);
		addCategoryPane.setBackground(new Color(50, 50, 50));
		addCategoryPane.addMouseListener(new MouseListener() {
			@Override public void mousePressed(MouseEvent e) { }
			@Override public void mouseClicked(MouseEvent e) { }
			@Override
			public void mouseEntered(MouseEvent e) {
				addCategoryPane.setBackground(new Color(70, 70, 70));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				addCategoryPane.setBackground(new Color(50, 50, 50));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				//TODO AddCategory
			}
		});
		categoryPane.add(addCategoryPane);
		
		lowerCategoryPane.setBounds(0, 102+screenSize.height/15, screenSize.width/6, screenSize.height-120);
		lowerCategoryPane.setOpaque(false);
		lowerCategoryPane.setLayout(null);
		lowerCategoryPane.setBorder(null);
		categoryPane.add(lowerCategoryPane);
		
		categoryPane.setBounds(0, 0, screenSize.width/6, screenSize.height);
		categoryPane.setBackground(new Color(50, 50, 50));
		contentPane.add(categoryPane);
		
		//-----------------------------------
		//			PartsPane
		//-----------------------------------
		
		JLabel lblParts = new JLabel("Parts:");
		lblParts.setFont(f.deriveFont(Font.BOLD).deriveFont(16f));
		lblParts.setBounds(20, 20, 60, 30);
		lblParts.setForeground(Color.WHITE);
		partPane.add(lblParts);
		
		JLabel lblPartsSeperator = new JLabel();
		lblPartsSeperator.setBounds(0, 70, screenSize.width/6, 1);
		lblPartsSeperator.setOpaque(true);
		lblPartsSeperator.setBackground(new Color(30, 30, 30));
		partPane.add(lblPartsSeperator);
		
		lowerPartPane.setBounds(0, 0, screenSize.width/6, screenSize.height-50);
		lowerPartPane.setOpaque(false);
		lowerPartPane.setBorder(null);
		lowerPartPane.setLayout(null);
		partPane.add(lowerPartPane);
		
		partPane.setBounds(screenSize.width/6, 0, screenSize.width/6, screenSize.height);
		partPane.setBackground(new Color(60, 60, 60));
		contentPane.add(partPane);
		
	}

}
