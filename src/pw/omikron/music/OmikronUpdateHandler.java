package pw.omikron.music;

import java.io.IOException;

import io.fouad.jtb.core.TelegramBotApi;
import io.fouad.jtb.core.UpdateHandler;
import io.fouad.jtb.core.beans.CallbackQuery;
import io.fouad.jtb.core.beans.ChosenInlineResult;
import io.fouad.jtb.core.beans.InlineQuery;
import io.fouad.jtb.core.beans.Message;
import io.fouad.jtb.core.builders.ApiBuilder;

public class OmikronUpdateHandler implements UpdateHandler {

	@Override
	public void onCallbackQueryReceived(TelegramBotApi arg0, int arg1, CallbackQuery arg2) {
		System.out.println(arg1);
		System.out.println(arg2.getFrom().getUsername());
		System.out.println(arg2.getMessage().getText());
	}

	@Override
	public void onChosenInlineResultReceived(TelegramBotApi arg0, int arg1, ChosenInlineResult arg2) {
		System.out.println(arg1);
		System.out.println(arg2.getQuery());
	}

	@Override
	public void onEditedMessageReceived(TelegramBotApi arg0, int arg1, Message arg2) {
		System.out.println(arg1);
		System.out.println(arg2.getFrom().getUsername());
		System.out.println(arg2.getCaption());
		System.out.println(arg2.getText());
	}

	@Override
	public void onGetUpdatesFailure(Exception arg0) {
		System.out.println(arg0.getLocalizedMessage());
	}

	@Override
	public void onInlineQueryReceived(TelegramBotApi arg0, int arg1, InlineQuery arg2) {
		System.out.println(arg1);
		System.out.println(arg2.getQuery());
	}

	@Override
	public void onMessageReceived(TelegramBotApi arg0, int arg1, Message arg2) {
		if(arg2.getFrom().getUsername().equals("IzOmikron")) {
			String msg = arg2.getText();
			String[] msgArray = msg.split(" ");
			if(msgArray.length >= 2) {
				if(msgArray[0].equals("/play_yt")) {
					try {
						Runtime.getRuntime().exec("/home/omikron/./yt2mp32cvlc.sh " + msgArray[1]);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				if(msgArray[0].equals("/stop")) {
					
				}
			}
		}
	}

}
